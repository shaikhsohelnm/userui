import React, { useState, useEffect } from "react";
import styled from "@emotion/styled";
import axios from "axios";
import Skeleton from "@mui/material/Skeleton";
import Stack from "@mui/material/Stack";
import CircularProgress from "@mui/material/CircularProgress";
import useMediaQuery from "@mui/material/useMediaQuery";

const WrapperStyled = styled.div`
  .support {
    overflow-y: scroll;
    width: 100%;
    height: 83vh;
    background: #c3b9ac;
    border: 5px solid #fff;
    max-width: 386px;
    margin: 0 auto;
    /* width */
    ::-webkit-scrollbar {
      width: 5px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      background: #ffb656;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: black;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #555;
    }
  }

  .userlist {
  }
  .profilecard {
    padding: 5px 8px;
    border: 3px solid #373753;
    margin: 12px auto;
    border-radius: 5px;
    width: 90%;
    img {
      width: 49px;
      height: 49px;
      border-radius: 10px;
    }
    p {
      margin: 0;
    }
  }

  h1 {
    margin: 0;
    text-align: center;
    margin-top: 8px;
  }
  .skeketonwrapper {
  }

  .userdetails {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    z-index: 100;
    background: #cbb1b1;
    width: 100%;
    height: 83vh;
    max-width: 386px;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    border: 12px solid #fff;
    color: black;
    padding: 0px 10px;
    p,
    input,
    label {
      padding: 0px;
      margin: 0px;
    }
    img {
      width: 100px;
      height: 100px;
      border-radius: 50%;
    }
    .form-group {
      margin: 10px 0px;
    }
    button {
      width: 100%;
      border: 0;
      outline: none;
      border-radius: 4px;
      max-width: 245px;
      color: #fff;
      background: black;
      font-size: 23px;
      margin-top: 15px;
    }
    input,
    textarea {
      color: #8a8a8a;
      font-size: 18px;
      font-weight: 500;
    }
    input {
      padding: 1px 12px;
    }
  }
  @media screen and (min-width: 991px) {
    .userdetails {
      position: unset !important;
    }

    display: flex;
    flex-direction: row;

    .closebtn {
      display: none;
    }
  }
`;

function UserUI() {
  const fallbackImage =
    "https://media.istockphoto.com/id/1495088043/vector/user-profile-icon-avatar-or-person-icon-profile-picture-portrait-symbol-default-portrait.jpg?s=612x612&w=0&k=20&c=dhV2p1JwmloBTOaGAtaA3AW1KSnjsdMt7-U_3EZElZ0=";

  const array50 = Array.from({ length: 50 }, (_, index) => index);
  const [userList, setUserList] = useState();
  const [userDetail, setUserDetails] = useState();
  const [popModal, setPopModal] = useState(0);
  const [imageLoading, setImageLoading] = useState(Array(50).fill(true));
  const [imageLoadingdDetail, setImageLoadingDetail] = useState(true);

  const isScreenGreaterThan991 = useMediaQuery("(min-width:991px)");

  useEffect(() => {
    setUserDetails({
      profile: {
        username: "no data",
        email: "Select a data from left",
      },
      Bio: "Select a data from left",
      avatar: "",
      fullName: "Select a data from left",
      jobTitle: "Select a data from left",
    });

    async function fetchData() {
      try {
        const response = await axios.get(
          "https://602e7c2c4410730017c50b9d.mockapi.io/users"
        );
        setUserList(response.data);
        console.log(response.data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }

    fetchData();
  }, []);

  useEffect(() => {
    if (isScreenGreaterThan991) {
      setPopModal(1);
    }
  }, [isScreenGreaterThan991]);

  const UserDetails = async (id) => {
    setPopModal(1);
    setImageLoadingDetail(true);
    setUserDetails(null);

    try {
      const response = await axios.get(
        `https://602e7c2c4410730017c50b9d.mockapi.io/users/${id}`
      );

      const {
        avatar,
        profile: { username, firstName, lastName, email },
        jobTitle,
        Bio,
      } = response.data;

      const fullName = `${firstName} ${lastName}`;

      setUserDetails({
        avatar,
        profile: {
          username,
          firstName,
          lastName,
          email,
        },
        jobTitle,
        Bio,
        fullName,
      });

      console.log(response.data);
    } catch (error) {
      console.error("Error fetching user details:", error);
    }
  };

  const closeModal = () => {
    setPopModal(0);
    setUserDetails(null);
    setImageLoadingDetail(true);
  };

  return (
    <>
      <h1>Users list</h1>

      <WrapperStyled className="position-relative main-wrapper">
        <div className="support">
          <div className="userlist">
            {userList
              ? userList.map((user, index) => (
                  <button
                    key={`${user.id}-${user.profile.username}`}
                    className="d-flex gap-2 flex-wrap align-items-center justify-content-start profilecard"
                    onClick={() => UserDetails(user.id)}
                  >
                    <img
                      src={user.avatar}
                      className="userprofile"
                      alt={`${user.profile.firstName}'s avatar`}
                      onLoad={() => {
                        setImageLoading((prev) => {
                          const newLoadingState = [...prev];
                          newLoadingState[index] = false;
                          return newLoadingState;
                        });
                      }}
                      onError={(e) => {
                        e.target.src = fallbackImage;
                        setImageLoading((prev) => {
                          const newLoadingState = [...prev];
                          newLoadingState[index] = false;
                          return newLoadingState;
                        });
                      }}
                    />

                    {imageLoading[index] && (
                      <CircularProgress color="inherit" />
                    )}
                    <p>{user.profile.firstName}</p>
                  </button>
                ))
              : array50.map((index) => (
                  <Stack
                    key={index}
                    direction="row"
                    spacing={2}
                    alignItems="center"
                  >
                    <div className="skeletonwrapper d-flex gap-2 flex-wrap align-items-center justify-content-start profilecard">
                      <Skeleton
                        variant="circular"
                        width={50}
                        height={50}
                        animation="wave"
                      />
                      <Skeleton
                        variant="rounded"
                        width={210}
                        height={60}
                        animation="wave"
                      />
                    </div>
                  </Stack>
                ))}
          </div>
        </div>
        {popModal === 1 && (
          <div className="userdetails">
            {userDetail ? (
              <>
                {userDetail.avatar && (
                  <img
                    src={userDetail.avatar}
                    className="userprofile"
                    alt={`${userDetail.profile.firstName}'s avatar`}
                    onLoad={() => {
                      setImageLoadingDetail(false);
                    }}
                    onError={(e) => {
                      setImageLoadingDetail(false);
                      e.target.src = fallbackImage;
                    }}
                  />
                )}
                {imageLoadingdDetail && <CircularProgress color="inherit" />}
                <p>@{userDetail.profile.username}</p>
                <div className="content">
                  <div class="form-group">
                    <textarea
                      type="text"
                      class="form-control"
                      value={userDetail.Bio}
                    />
                  </div>

                  <div class="form-group">
                    <label for="fullName">Full Name</label>
                    <input
                      type="text"
                      class="form-control"
                      value={userDetail.fullName}
                    />
                  </div>
                  <div class="form-group">
                    <label for="FullName"> Job Title</label>
                    <input
                      type="text"
                      class="form-control"
                      value={userDetail.jobTitle}
                    />
                  </div>
                  <div class="form-group">
                    <label for="FullName"> Email</label>
                    <input
                      type="text"
                      class="form-control"
                      value={userDetail.profile.email}
                    />
                  </div>
                </div>
                <button className="closebtn" onClick={closeModal}>
                  Close
                </button>
              </>
            ) : (
              <>
                <Stack direction="column" spacing={2} alignItems="center">
                  <Skeleton
                    variant="circular"
                    width={100}
                    height={100}
                    animation="wave"
                  />
                  <Skeleton
                    variant="rectangular"
                    width={210}
                    height={20}
                    animation="wave"
                  />
                  <Skeleton
                    variant="rectangular"
                    width={210}
                    height={60}
                    animation="wave"
                  />
                  <Skeleton
                    variant="rectangular"
                    width={210}
                    height={30}
                    animation="wave"
                  />
                  <Skeleton
                    variant="rectangular"
                    width={210}
                    height={30}
                    animation="wave"
                  />
                  <Skeleton
                    variant="rectangular"
                    width={210}
                    height={30}
                    animation="wave"
                  />
                </Stack>
                <button className="closebtn" onClick={closeModal}>
                  Close
                </button>
              </>
            )}
          </div>
        )}
      </WrapperStyled>
    </>
  );
}

export default UserUI;
