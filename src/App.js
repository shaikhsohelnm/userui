import "./App.css";
import UserUI from "./pages/UserUI";

function App() {
  return (
    <div className="App">
      <UserUI />
    </div>
  );
}

export default App;
